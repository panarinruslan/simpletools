"""
Crop raster by polygon (single object in file)
Output: single file #name_cropped.tif
"""

import os
import sys
import pathlib
import click
from osgeo import gdal, ogr


class HelpfulCmd(click.Command):
    def format_help(self, ctx, formatter):
        text = """Crops raster by provided polygon mask

Usage: crop_raster.py [OPTIONS]
Options:
  --raster TEXT   path to raster file to be cropped
  --polygon TEXT  path to polygon file to used as a mask
  --outdir TEXT   path to raster file to be cropped
  --outname TEXT  path to polygon file to used as a mask
  --help          Show this message and exit.
"""
        click.echo(text)


class CropRaster:
    def __init__(self, raster, polygon, outdir, outname):
        self.raster = raster
        self.polygon = polygon
        self.outdir = outdir
        self.outname = outname

    @property
    def raster(self):
        return self._raster

    @raster.setter
    def raster(self, raster):
        try:
            raster_data = gdal.Open(raster)
        except ValueError:
            raster_data = None

        if raster_data:
            self._raster = raster
        else:
            raise ValueError(f"{raster} not valid")

    @property
    def polygon(self):
        return self._polygon

    @polygon.setter
    def polygon(self, polygon):

        try:
            data = ogr.Open(str(polygon))
        except ValueError:
            data = None

        if data:
            lyr = data.GetLayerByIndex(0)

            if len(lyr) != 1:
                raise ValueError(f"{polygon} has more than one object")

            feature = lyr.GetNextFeature()
            geometry = feature.GetGeometryRef()

            if geometry.GetGeometryName() != "MULTIPOLYGON" and geometry.GetGeometryName() != "POLYGON":
                raise ValueError(f"{polygon} not a polygon")

            self._polygon = polygon
        else:
            raise ValueError(f"Can't open file {polygon}")

    @property
    def outdir(self):
        return self._outdir

    @outdir.setter
    def outdir(self, outdir):
        if outdir is not None:
            if pathlib.Path(outdir).is_dir():
                self._outdir = outdir
            else:
                raise ValueError(f"{outdir} does not exist")
        else:
            self._outdir = None

    @property
    def outname(self):
        return self._outname

    @outname.setter
    def outname(self, outname):
        if outname is not None:
            self._outname = outname
        else:
            self._outname = None

    def crop_raster(self) -> str:
        if self.outdir and self.outname:
            out_raster = os.path.join(self.outdir, self.outname)

        elif self.outdir:
            out_name = (
                os.path.splitext(os.path.basename(self.raster))[0]
                + "_cropped"
                + os.path.splitext(os.path.basename(self.raster))[1]
            )
            out_raster = os.path.join(self.outdir, out_name)

        else:
            out_raster = os.path.splitext(self.raster)[0] + "_cropped" + os.path.splitext(self.raster)[1]

        try:
            gdal.Warp(out_raster, self.raster, cutlineDSName=self.polygon, cropToCutline=True)
            return out_raster
        except SystemExit:
            return f"Can't crop raster {self.raster} by {self.polygon}"


@click.command(cls=HelpfulCmd)
@click.option("--raster", help="path to raster file to be cropped")
@click.option("--polygon", help="path to polygon file to used as a mask")
@click.option("--outdir", help="Optional: out directory")
@click.option("--outname", help="Optional: out filename")
def main(raster, polygon, outdir=None, outname=None):
    sys.tracebacklimit = 0
    crop_pool = CropRaster(raster, polygon, outdir, outname)
    result = crop_pool.crop_raster()
    print(result)


if __name__ == "__main__":
    # pylint: disable=[no-value-for-parameter]
    main()
