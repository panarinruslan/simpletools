certifi==2021.5.30
click @ file:///C:/ci/click_1646056762388/work
colorama @ file:///tmp/build/80754af9/colorama_1607707115595/work
GDAL==3.4.1
mkl-fft==1.3.1
mkl-random @ file:///C:/ci_310/mkl_random_1643050563308/work
mkl-service==2.4.0
numpy @ file:///C:/ci/numpy_and_numpy_base_1650959319189/work
six @ file:///tmp/build/80754af9/six_1644875935023/work
wincertstore==0.2
